# -*- coding: utf-8 -*-
try:
    from kafka import KafkaProducer
    HAS_LIBS = (True,)
except ImportError:
    HAS_LIBS = False, "kafka is not available"


def __virtual__(hub):
    return HAS_LIBS


async def publish(hub, ctx, event):
    if not ctx and not ctx.connected:
        return

    for tag in ctx.tags:
        ctx.connection.send(tag, event)
