# -*- coding: utf-8 -*-
try:
    from kafka import KafkaProducer
    HAS_LIBS = (True,)
except ImportError:
    HAS_LIBS = False, "kafka is not available"


async def gather(hub, profiles):
    """
    Get profile data

    Example:

    .. code-block:: yaml

        kafka:
          profile_name:
            bootstrap_servers:
              - localhost:9092
            tags:
              - tag1
              - tag2
    """
    sub_profiles = {}
    for (
        profile,
        ctx,
    ) in profiles.get("kafka", {}).items():
        sub_profiles[profile] = {
            "connected": False,
            "tags": ctx.pop("tags", []),
            "connection": None,
        }

        hub.log.debug("connecting to kafka server")
        try:
            sub_profiles[profile]["connection"] = KafkaProducer(
                bootstrap_servers=ctx["bootstrap_servers"]
            )
            sub_profiles[profile]["connected"] = True
            hub.log.debug("connected to kafka servers")
        except Exception as err:
            hub.log.error("Could not connect to kafka servers")
            hub.log.error(str(err))

    return sub_profiles
